import os
import gitlab


gitlab_url = 'https://gitlab.com/'
#token = 'glpat-zZ4hqfHj6hyyxSx4_FGZ'
token = input("Please enter your token!")

#project_name = 'Our Project2'
project_name = input("Please enter your Project name: ")
project_description = 'A template for projects.'
project_visibility = 'public' 

gl = gitlab.Gitlab(gitlab_url, private_token=token)
project = gl.projects.create({
    'name': project_name,
    'description': project_description,
    'visibility': project_visibility,
})

print(f"Project created: {gitlab_url}{project_name}")


if not os.path.exists('.git'):
    os.system('git init')

os.system('git add .') 

os.system('git commit -m "Initial commit"')

try:
    os.system(f'git remote set-url origin {project.http_url_to_repo}')
finally:
    os.system(f'git remote add origin {project.http_url_to_repo}')


os.system('git push -u origin master')


